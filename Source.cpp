#include <iostream>
#include <fstream>
#include <string>
#include <map>

#include <xmlpull/XmlPullParser.h>
#include <xmlpull/XmlPullParserException.h>
#include <schemaparser/SchemaParser.h>
#include <schemaparser/SchemaParserException.h>
#include <wsdlparser/WsdlParser.h>
#include <wsdlparser/Soap.h>
#include <wsdlparser/WsdlInvoker.h>

#include <tinyxml2.h>

using namespace std;
using namespace WsdlPull;
using namespace tinyxml2;

int main() {
	setlocale(LC_ALL, "rus");

	string URI = "http://www.webservicex.com/globalweather.asmx?WSDL";

	WsdlInvoker invoker;

	std::vector<std::string> operations;

	if (!invoker.setWSDLUri(URI)) {
		cout << "Error: processing " << URI << endl;
		cout << invoker.errors() << endl;
		return 1;
	}
	cout << "������������ � " << URI << endl;

	operations.clear();
	if (!invoker.getOperations(operations)) {
		cout << "Error: No operation found or missing <binding> section." << endl;
		return 2;
	}
	cout << "��������� �������: ";
	for (auto func : operations)
		cout << func << " ";
	cout << endl;

	if (!invoker.setOperation("GetCitiesByCountry")) {
		cout << "������ �� ������������ �������� GetCitiesByCountry " << endl;
		return 1;
	}

	string cName;
	cout << "������� �������� ������ (����������): ";
	cin >> cName;

	if (!invoker.setInputValue(0, cName)) {
			cout << "�������� ��������" << endl;
	}

	map<string, string> response;

	if (invoker.invoke()) {
		response.clear();

		std::string outParamName;
		TypeContainer* tc = 0;

		while (invoker.getNextOutput(outParamName, tc)) {
			ostringstream outValue;
			tc->print(outValue);
			response[outParamName] = outValue.str();
		}
	}
	else {
		cout << "������ ��� �������� �������." << endl;
		return 3;
	}

	string xmlResponse = response.begin()->second;
	
	vector<string> cities;
	try {
		XMLDocument doc;
		doc.Parse(xmlResponse.c_str());
		XMLElement* table = doc.FirstChildElement()->FirstChildElement();
		XMLElement* country = table->FirstChildElement("Country");
		cName = country->GetText();
		cout << cName << endl;
		for (table; table != 0; table = table->NextSiblingElement()) {

			XMLElement* city = table->FirstChildElement("City");
			cities.push_back(city->GetText());
		}
	}
	catch (...) {
		cout << "������! ������� ���������� �������� ������." << endl;
		return 4;
	}

	cout << "������ �������:" << endl;
	for (int i = 0; i < cities.size(); ++i)
		cout << i << ". " << cities[i] << endl;

	cout << "\n�������� ����� (�����): ";
	int N;
	cin >> N;

	if (cities.size() > N && N >= 0) {
		if (!invoker.setOperation("GetWeather")) {
			cout << "������ �� ������������ �������� GetWeather " << endl;
			return 1;
		}
		invoker.setInputValue(1, cName);
		invoker.setInputValue(0, cities[N]);

		if (invoker.invoke()) {
			response.clear();

			std::string outParamName;
			TypeContainer* tc = 0;

			while (invoker.getNextOutput(outParamName, tc)) {
				ostringstream outValue;
				tc->print(outValue);
				response[outParamName] = outValue.str();
			}
		}
		else {
			cout << "������ ��� �������� �������." << endl;
			return 3;
		}

		for (auto s : response)
			cout << s.second << endl;
	}

	return 0;
}