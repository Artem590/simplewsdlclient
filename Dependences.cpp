//Dependences and defenitions for build Wsdlpull

#ifdef _WIN32
#pragma comment(lib, "MSVCRTD.lib")
#pragma comment(lib, "legacy_stdio_definitions.lib")
#pragma comment(lib, "ws2_32.lib")
#include <stdio.h>
#include <Windows.h>
extern "C" { FILE __iob_func[3] = { *stdin, *stdout, *stderr }; }
#endif

#pragma comment(lib, "libcurl_a.lib")